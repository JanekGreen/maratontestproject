
package pl.nysa.hackaton.testproject.Entity;

import java.util.HashMap;
import java.util.Map;

public class Activity {

    private String id;
    private String schoolID;
    private String weekDayID;
    private String start;
    private String stop;
    private String name;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public String getWeekDayID() {
        return weekDayID;
    }

    public void setWeekDayID(String weekDayID) {
        this.weekDayID = weekDayID;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getStop() {
        return stop;
    }

    public void setStop(String stop) {
        this.stop = stop;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
