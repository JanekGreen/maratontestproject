package pl.nysa.hackaton.testproject.networktools;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

import pl.nysa.hackaton.testproject.interfaces.RequestInterface;
import pl.nysa.hackaton.testproject.ui.LoginActivity;
import pl.nysa.hackaton.testproject.ui.MainMapActivity;

/**
 * Created by wojci on 28.10.2017.
 */

public class RequestSender {

    public static void sendRequest(String url, final Activity activity){
        final ProgressDialog progressDialog = ProgressDialog.show(activity,null,"Proszę czekać...");
        RequestHandler rq = RequestHandler.getInstance(activity);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                ((RequestInterface)activity).onResponse(response);
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                ((RequestInterface)activity).onErrorResponse(error);
                progressDialog.dismiss();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                return ((RequestInterface)activity).getParams();
            }
        };

        rq.getRequestQueue().add(stringRequest);
    }

    }