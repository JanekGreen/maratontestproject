package pl.nysa.hackaton.testproject.ui;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.nysa.hackaton.testproject.Adapter.EventListAdapter;
import pl.nysa.hackaton.testproject.Adapter.LineListAdapter;
import pl.nysa.hackaton.testproject.Entity.Activity;
import pl.nysa.hackaton.testproject.Entity.Event;
import pl.nysa.hackaton.testproject.R;
import pl.nysa.hackaton.testproject.interfaces.ListInterface;
import pl.nysa.hackaton.testproject.interfaces.RequestInterface;
import pl.nysa.hackaton.testproject.networktools.RequestHandler;
import pl.nysa.hackaton.testproject.networktools.RequestSender;
import pl.nysa.hackaton.testproject.networktools.ResponseHandler;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainListFragment extends Fragment implements RequestInterface, ListInterface{

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private ArrayList<Event> listOfEvents;
    private EventListAdapter eventListAdapter;
    public MainListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_event_list, container, false);
        ButterKnife.bind(this,v);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        RequestSender.sendRequest(RequestHandler.EVENTS_REQUEST,getActivity());
    }

    @Override
    public void onResponse(String response) {
//        ResponseHandler.saveEvents(response);
//        listOfEvents = ResponseHandler.getEvents();
        if(listOfEvents != null) {
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
            eventListAdapter = new EventListAdapter(null, listOfEvents,this, getContext());
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(eventListAdapter);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public Map<String, String> getParams() throws AuthFailureError {
        return null;
    }

    @Override
    public void onItemClick(Activity activity, View v) {

    }
}
