package pl.nysa.hackaton.testproject.database;

import android.content.Context;
import android.os.Environment;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;

/**
 * Created by wojci on 29.10.2017.
 */

public class DatabaseHandler {
    public static DatabaseHandler databaseHandler;
    private static ObjectContainer db;
    private static Context context;
    private DatabaseHandler(Context context){
         databaseHandler  =  getDatabaseHandler();
         this.context = context;
    }

    public static synchronized ObjectContainer getObjectContainer(){
        if( db == null){
            db =  Db4oEmbedded.openFile("/data/user/0/pl.nysa.hackaton.testproject/app_data/DBMAIN");
        }
        return db;
    }
     private DatabaseHandler getDatabaseHandler(){
        if(databaseHandler == null){
            databaseHandler = new DatabaseHandler(context);
     }
        return databaseHandler;
 }




}
