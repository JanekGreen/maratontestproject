package pl.nysa.hackaton.testproject.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.nysa.hackaton.testproject.R;
import pl.nysa.hackaton.testproject.Utils.DialogService;
import pl.nysa.hackaton.testproject.interfaces.DialogServiceInterface;

public class NewEventActivity extends AppCompatActivity implements DialogServiceInterface{

    @BindView(R.id.newEventDate)
    EditText newEventDate;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private DialogService dialogService;
    private Date newDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_event);
        ButterKnife.bind(this);
        dialogService = new DialogService(this, this, 0);
        setDateTimeEditText();
        newDate = new Date();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


    }
    private void setDateTimeEditText(){
        newEventDate.setFocusable(false);
        newEventDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(MotionEvent.ACTION_UP == event.getAction()) {
                    dialogService.showDatePicker(Calendar.getInstance(), "Wybierz dzień");
                }

                return true;
            }
        });

    }

    @Override
    public void onClickOK(int code) {

    }

    @Override
    public void onClickCancel(int code) {

    }

    @Override
    public void onClickDatePickter(int year, int monthOfYear, int dayOfMonth) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, monthOfYear, dayOfMonth);

        newDate =  calendar.getTime();

        dialogService.showTimePicker(Calendar.getInstance(), "Wybierz godzinę");
    }

    @Override
    public void onClickTimePicker(int hourOfDay, int minute) {
        newDate.setHours(hourOfDay);
        newDate.setMinutes(minute);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateStr = sdf.format(newDate);
        newEventDate.setText(dateStr);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
