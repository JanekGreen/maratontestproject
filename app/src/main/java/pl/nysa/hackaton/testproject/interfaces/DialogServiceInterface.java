package pl.nysa.hackaton.testproject.interfaces;

/**
 * Created by akufa on 2017-10-29.
 */

public interface DialogServiceInterface {

    public void onClickOK(int code);
    public void onClickCancel(int code);
    public void onClickDatePickter(int year, int monthOfYear, int dayOfMonth);
    public void onClickTimePicker(int hourOfDay, int minute);
}
