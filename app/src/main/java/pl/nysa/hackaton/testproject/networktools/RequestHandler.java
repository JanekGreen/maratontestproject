package pl.nysa.hackaton.testproject.networktools;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.toolbox.Volley;

/**
 * Created by wojci on 28.10.2017.
 */

public class RequestHandler {

    private  com.android.volley.RequestQueue requestQueue;
    private static Context mCtx;
    private static RequestHandler rqInstance;
    public static String SERVER_URL ="http://hackathon.nyskaarenagier.pl/public/api/";
    public static String EVENTS_REQUEST ="http://hackathon.nyskaarenagier.pl/public/api/events";
    public static String WEEKDAYS_REQUEST ="http://hackathon.nyskaarenagier.pl/public/api/week_days";
    public static String REGISTER_REQUEST ="http://hackathon.nyskaarenagier.pl/public/api/register";
    public static String LOGIN_REQUEST ="http://hackathon.nyskaarenagier.pl/public/api/login";

    private RequestHandler(Context mCtx){
        this.mCtx = mCtx;
        requestQueue = getRequestQueue();

    }
    public  com.android.volley.RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return requestQueue;
    }
    public static synchronized RequestHandler getInstance(Context context) {
        if (rqInstance== null) {
             rqInstance = new RequestHandler(context);
        }
        return rqInstance;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }
}
