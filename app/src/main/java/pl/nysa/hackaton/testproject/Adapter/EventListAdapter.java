package pl.nysa.hackaton.testproject.Adapter;

import android.content.Context;
import android.os.Build;
import android.provider.CalendarContract;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.nysa.hackaton.testproject.Entity.Activity;
import pl.nysa.hackaton.testproject.Entity.Event;
import pl.nysa.hackaton.testproject.Entity.Line;
import pl.nysa.hackaton.testproject.Entity.School;
import pl.nysa.hackaton.testproject.Entity.User;
import pl.nysa.hackaton.testproject.R;
import pl.nysa.hackaton.testproject.database.DatabaseHandler;
import pl.nysa.hackaton.testproject.interfaces.ListInterface;
import pl.nysa.hackaton.testproject.interfaces.ListListener;

/**
 * Created by akufa on 2017-10-29.
 */

public class EventListAdapter extends RecyclerView.Adapter<EventListAdapter.ViewHolder> {
    Context context;
    List<Activity> listOfActivities;
    List<Event> listOfEvents;
    ListInterface listClickInterface;

    public EventListAdapter(List<Activity> listOfActivities, List<Event> listOfEvents, ListInterface listClickInterface, Context context) {
        this.context = context;
        this.listOfActivities = listOfActivities;
        this.listOfEvents = listOfEvents;
        this.listClickInterface = listClickInterface;
    }


    @Override
    public EventListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_event_list, parent, false);

        return new EventListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EventListAdapter.ViewHolder holder, int position) {
        if(listOfActivities!= null && !listOfActivities.isEmpty() && position < listOfActivities.size()) {
            Activity activity = listOfActivities.get(position);
            //relation activity - school
            School school = getRelatedSchool(activity);
            //

            holder.schoolTitle.setText(school.getTitle());
            holder.eventDescription.setText(activity.getName());
            String timeStart, timeEnd;
            timeStart = activity.getStart();
            timeEnd = activity.getStop();
            if(!timeEnd.isEmpty())
                holder.eventTime.setText(timeStart+" - "+timeEnd);
            else
                holder.eventTime.setText(timeStart);

        }
        else if(listOfEvents != null && !listOfEvents.isEmpty()){
            Event event = listOfEvents.get(position);
            User user = getRelatedUser(event);
            holder.schoolTitle.setText(user.getFirstname().substring(0,1)+" "+user.getLastname().substring(0,1));  //dla jednej literki imienia i nazwiska
            holder.eventDescription.setText(event.getName());
            DateFormat df = new SimpleDateFormat("yyyy-mm-dd MM:hh:ss");
            Date result = null;
            try {
                result = df.parse(event.getDate());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            holder.eventTime.setText(String.valueOf(result.getHours())+":"+String.valueOf(result.getMinutes()));
        }
     //   ViewCompat.setTransitionName(holder.lineCircle, line.getAddress());
    }

    private User getRelatedUser(Event event){
        String userID = event.getUserId();
        User user = new User();
        user.setID(userID);
        ObjectContainer objectContainer = DatabaseHandler.getObjectContainer();
        ObjectSet objects = objectContainer.queryByExample(user);
        User result = (User) objects.get(0);
        return result;
    }

    private School getRelatedSchool(Activity activity){
        String schoolID = activity.getSchoolID();
        School school = new School();
        school.setSchoolTypeID(schoolID);
        ObjectContainer objectContainer = DatabaseHandler.getObjectContainer();
        ObjectSet objects = objectContainer.queryByExample(school);
       School result = (School) objects.get(0);
        return result;
    }

    @Override
    public int getItemCount() {
        if(listOfActivities == null && listOfEvents == null)
            return 0;
        else
            return listOfActivities.size() + listOfEvents.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.circleImg2)
        CardView lineCircle;
        @BindView(R.id.schoolTitle)
        TextView schoolTitle;
        @BindView(R.id.schoolName)
        TextView schoolName;
        @BindView(R.id.eventDescription)
        TextView eventDescription;
        @BindView(R.id.eventTime)
        TextView eventTime;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listClickInterface.onItemClick(null, lineCircle);
        }
    }
}
