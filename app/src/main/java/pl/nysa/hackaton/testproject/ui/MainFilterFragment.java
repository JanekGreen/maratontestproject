package pl.nysa.hackaton.testproject.ui;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.nysa.hackaton.testproject.R;
import pl.nysa.hackaton.testproject.Utils.DialogService;
import pl.nysa.hackaton.testproject.interfaces.DialogServiceInterface;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFilterFragment extends Fragment implements DialogServiceInterface {

    @BindView(R.id.filterEventType)
    Spinner filterEventType;
    @BindView(R.id.filterSchoolType)
    Spinner filterSchoolType;
    @BindView(R.id.filterDate)
    EditText filterDate;

    private DialogService dialogService;
    private Date dateFilter;

    public MainFilterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_main_filter, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        dialogService = new DialogService(getContext(), this, 0);
        dateFilter = new Date();
        setSpinners();
        setDateTimeEditText();
    }

    private void setDateTimeEditText(){
        filterDate.setFocusable(false);
        filterDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(MotionEvent.ACTION_UP == event.getAction()) {
                   dialogService.showDatePicker(Calendar.getInstance(), "Wybierz dzień");
                }

                return true;
            }
        });

    }



    private void setSpinners(){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, generatePlaceholders(1));
        filterSchoolType.setAdapter(adapter);
        filterSchoolType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View arg1, int arg2,
                                       long arg3) {
                ((TextView) parent.getChildAt(0)).setTextColor(getContext().getResources().getColor(R.color.colorSecondaryText));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, generatePlaceholders(2));
        filterEventType.setAdapter(adapter2);
        filterEventType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View arg1, int arg2,
                                       long arg3) {
                ((TextView) parent.getChildAt(0)).setTextColor(getContext().getResources().getColor(R.color.colorSecondaryText));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        filterEventType.setSelection(0);
        filterSchoolType.setSelection(0);
    }

    private String[] generatePlaceholders(int i){
        //School
        String[] arraySpinner = null;
        if(i==1) {
            arraySpinner = new String[]{
                  "",  "Szkoła podstawowa", "Szkoła średnia", "Szkoła wyższa"
            };
        }
        //Event type
        if(i==2) {
            arraySpinner = new String[]{
                   "", "Szkolny", "Z miasta", "Użytkowników"
            };
        }
        return arraySpinner;
    }

    @Override
    public void onClickOK(int code) {

    }

    @Override
    public void onClickCancel(int code) {

    }

    @Override
    public void onClickDatePickter(int year, int monthOfYear, int dayOfMonth) {

            Calendar calendar = Calendar.getInstance();
            calendar.set(year, monthOfYear, dayOfMonth);

            dateFilter =  calendar.getTime();

        dialogService.showTimePicker(Calendar.getInstance(), "Wybierz godzinę");
    }

    @Override
    public void onClickTimePicker(int hourOfDay, int minute) {
        dateFilter.setHours(hourOfDay);
        dateFilter.setMinutes(minute);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateStr = sdf.format(dateFilter);
        filterDate.setText(dateStr);
    }


}
