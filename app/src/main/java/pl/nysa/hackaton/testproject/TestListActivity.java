package pl.nysa.hackaton.testproject;

import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.nysa.hackaton.testproject.Adapter.LineListAdapter;
import pl.nysa.hackaton.testproject.Entity.Line;
import pl.nysa.hackaton.testproject.interfaces.ListListener;

public class TestListActivity extends AppCompatActivity implements ListListener {

    @BindView(R.id.linesList)
    RecyclerView listLines;

    private List<Line> testLines;
    private LineListAdapter lineListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_list);
        ButterKnife.bind(this);
        prepareData();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        lineListAdapter = new LineListAdapter(testLines, this, this);
        listLines.setLayoutManager(layoutManager);
        listLines.setAdapter(lineListAdapter);

    }


    private void prepareData(){
        testLines = new ArrayList<>();
        testLines.add(new Line("1", "Kolejowa", "1.02 km", "12:10"));
        testLines.add(new Line("3", "Mickiewicza", "0.6 km", "11:55"));
        testLines.add(new Line("7", "Poniatowskiego", "2.6 km", "14:55"));
        testLines.add(new Line("8", "Rynek", "1.0 km", "13:05"));
        testLines.add(new Line("8", "Zajezdnia", "1.8 km", "10:05"));
    }


    @Override
    public void onItemClick(Line line, CardView view) {
        System.out.println("OnItemClick");
        Intent intent = new Intent(this, LineDetailsActivity.class);
        intent.putExtra("EXTRA_LINE_ITEM", line);
        intent.putExtra("EXTRA_ANIMAL_IMAGE_TRANSITION_NAME", ViewCompat.getTransitionName(view));

        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this,
                view,
                ViewCompat.getTransitionName(view));

        startActivity(intent, options.toBundle());

    }
}
