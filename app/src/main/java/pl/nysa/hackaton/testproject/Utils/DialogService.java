package pl.nysa.hackaton.testproject.Utils;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.DatePicker;
import android.widget.TimePicker;

import java.util.Calendar;

import pl.nysa.hackaton.testproject.R;
import pl.nysa.hackaton.testproject.interfaces.DialogServiceInterface;

/**
 * Created by akufa on 2017-10-29.
 */

public class DialogService {
    private Context context;
    private DialogServiceInterface dialogServiceInterface;
    private int code;

    public DialogService(Context context, DialogServiceInterface dialogServiceInterface,int code) {
        this.context = context;
        this.dialogServiceInterface = dialogServiceInterface;
        this.code = code;
    }

    public void showDatePicker(Calendar calendar, String title){
        final DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                dialogServiceInterface.onClickDatePickter(year, monthOfYear, dayOfMonth);
            }
        };
        DatePickerDialog datedialog = new DatePickerDialog(context,
                listener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datedialog.setTitle(title);
        datedialog.show();
    }

    public void showTimePicker(Calendar calendar, String title){

            TimePickerDialog.OnTimeSetListener listener = new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    dialogServiceInterface.onClickTimePicker(hourOfDay, minute);
                }
            };

            TimePickerDialog timedialog = new TimePickerDialog(context,
                    listener, Calendar.HOUR_OF_DAY, Calendar.MINUTE, true);
            timedialog.setTitle(title);
            timedialog.show();
    }

    public void showOKDialog(String title, String message){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this.context);

            alertDialogBuilder.setTitle(title);
            alertDialogBuilder.setMessage(message);

            alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (dialogServiceInterface != null)
                        dialogServiceInterface.onClickOK(code);
                    dialog.cancel();
                }
            });
            AlertDialog alert = alertDialogBuilder.create();
            alert.show();
    }


}
