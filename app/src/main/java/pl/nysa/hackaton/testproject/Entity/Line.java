package pl.nysa.hackaton.testproject.Entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by artur on 25.10.2017.
 * Stworzone na potrzeby testowe. Zostanie zastąpione jakims wymodelowanym entity z orm-a.
 */

public class Line implements Parcelable {

    private String number; //Wrazie jakby sie pojawiła linia 1A czy coś podobnego
    private String address;
    private String distance;
    private String startTime;
    private int lineColor;
    //...

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Line> CREATOR = new Parcelable.Creator<Line>() {
        @Override
        public Line createFromParcel(Parcel in) {
            return new Line(in);
        }

        @Override
        public Line[] newArray(int size) {
            return new Line[size];
        }
    };


    public Line(String number, String address, String distance, String startTime) {
        this.number = number;
        this.address = address;
        this.distance = distance;
        this.startTime = startTime;
    }

    public String getNumber() {
        return number;
    }

    public String getAddress() {
        return address;
    }

    public String getDistance() {
        return distance;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setLineColor(int lineColor) {
        this.lineColor = lineColor;
    }

    public int getLineColor() {
        return lineColor;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(number);
        dest.writeString(address);
        dest.writeString(distance);
        dest.writeString(startTime);
        dest.writeInt(lineColor);

    }

    protected Line(Parcel in) {
        number = in.readString();
        address = in.readString();
        distance = in.readString();
        startTime = in.readString();
        lineColor = in.readInt();
    }
}


