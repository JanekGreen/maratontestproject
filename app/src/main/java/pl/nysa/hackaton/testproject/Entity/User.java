
package pl.nysa.hackaton.testproject.Entity;



import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.types.IntegerObjectType;

import java.util.HashMap;
import java.util.Map;


public class User{

    @DatabaseField(id = true)
    private int id;
    @DatabaseField
    private String username;
    @DatabaseField
    private String firstname;
    @DatabaseField
    private String lastname;
    @DatabaseField
    private String email;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getID() {
        return String.valueOf(id);
    }

    public void setID(String iD) {
        this.id = Integer.parseInt(iD);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
