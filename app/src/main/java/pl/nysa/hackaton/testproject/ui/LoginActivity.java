package pl.nysa.hackaton.testproject.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.VolleyError;
import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;
import com.j256.ormlite.dao.RuntimeExceptionDao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.nysa.hackaton.testproject.Entity.User;
import pl.nysa.hackaton.testproject.Entity.WeekDay;
import pl.nysa.hackaton.testproject.R;
import pl.nysa.hackaton.testproject.database.DatabaseHandler;
import pl.nysa.hackaton.testproject.database.DatabaseHelper;
import pl.nysa.hackaton.testproject.interfaces.RequestInterface;
import pl.nysa.hackaton.testproject.networktools.RequestHandler;
import pl.nysa.hackaton.testproject.networktools.RequestSender;
import pl.nysa.hackaton.testproject.networktools.ResponseHandler;

public class LoginActivity extends OrmLiteBaseActivity<DatabaseHelper> implements RequestInterface{

    @BindView(R.id.logoText)
    TextView logoText;
    @BindView(R.id.username)
    EditText etUsername;
    @BindView(R.id.password)
    EditText etPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        etUsername.setText("pwojcik");
        etPassword.setText("jajajja");


        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "logoFont.ttf");

        logoText.setTypeface(custom_font);
        logoText.setTextColor(getResources().getColor(R.color.iconWhite));
        logoText.setTextSize(61);
    }

    @OnClick(R.id.signUp)
    public void onSignUp(){
        Intent intent = new Intent(this, SignUpActivity.class);
        startActivity(intent);

    }

    @OnClick(R.id.fbLoginButton)
    public void onFbLoginButton(){
        Intent intent = new Intent(LoginActivity.this, SchoolDetailsActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.loginButton)
    public void onLoginButton(){

        RequestSender.sendRequest(RequestHandler.LOGIN_REQUEST,this);
    }

    @Override
    public void onResponse(String response) {
        showMessage("Pomyślnie zalogowano!");
        System.out.println(response);
      //  ResponseHandler.saveUser(response);
       List<User> users =  ResponseHandler.getUsers(response);
        RuntimeExceptionDao<User, Integer> userDao = getHelper().getSimpleDataDao();
        userDao.create(users);
        List<User> listFromDB = userDao.queryForAll();
        System.out.println(listFromDB.size());
        Intent intent = new Intent(LoginActivity.this,MainMapActivity.class);
        startActivity(intent);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        showMessage("Wystąpił błąd podczas połączenia z serwerem "+error.getMessage());
        System.err.println("Error during registration: "+error.getMessage());
    }

    @Override
    public Map<String, String> getParams() throws AuthFailureError {
        HashMap<String,String> params = new HashMap<>();
        params.put("login",etUsername.getText().toString());
        params.put("password",etPassword.getText().toString());

        return params;
    }

    private void showMessage(String message){
        Toast.makeText(this,message,Toast.LENGTH_LONG).show();
    }
}
