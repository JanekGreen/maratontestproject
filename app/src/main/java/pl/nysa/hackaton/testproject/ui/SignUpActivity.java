package pl.nysa.hackaton.testproject.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.VolleyError;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.nysa.hackaton.testproject.R;
import pl.nysa.hackaton.testproject.Utils.DialogService;
import pl.nysa.hackaton.testproject.interfaces.DialogServiceInterface;
import pl.nysa.hackaton.testproject.interfaces.RequestInterface;
import pl.nysa.hackaton.testproject.networktools.RequestHandler;
import pl.nysa.hackaton.testproject.networktools.RequestSender;

public class SignUpActivity extends AppCompatActivity implements RequestInterface, DialogServiceInterface {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.signInPassword)
    EditText signInPassword;
    @BindView(R.id.signInRepeatPassword)
    EditText signInRepeatPassword;
    @BindView(R.id.signInUsername)
    EditText signInUsername;
    @BindView(R.id.signInMail)
    EditText signInMail;
    @BindView(R.id.agreementCheckbox)
    CheckBox agreementCheckbox;

    private static final int NO_AGREEMENT_CHECKED=1;
    private DialogService dialogService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        signInUsername.setText("pwojcik");
        signInPassword.setText("jajajja");
        signInMail.setText("pwojcik@mail.com");
        dialogService = new DialogService(this, this, NO_AGREEMENT_CHECKED);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @OnClick(R.id.signUpButton)
    public void onSignUpButton(){
        if(!checkPasswords()){
            signInPassword.setError("Hasła nie są takie same.");
            signInRepeatPassword.setError("Hasła nie są takie same.");
        }else if(agreementCheckbox.isChecked()){
            RequestSender.sendRequest(RequestHandler.REGISTER_REQUEST,this);
        }
        else{
            dialogService.showOKDialog("Błąd", "By móc zarejestrować nowe konto należy zaakceptować warunki umowy.");
        }

    }

    private boolean checkPasswords(){
        if(signInPassword.getText().toString().equals(signInRepeatPassword.getText().toString()))
            return true;
        return false;
    }

    @Override
    public void onResponse(String response) {
        if(response.equalsIgnoreCase("OK")){
            Intent intent = new Intent(SignUpActivity.this,LoginActivity.class);
            startActivity(intent);
        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public Map<String, String> getParams() throws AuthFailureError {
        HashMap<String,String> params = new HashMap<>();
        params.put("login",signInUsername.getText().toString());
        params.put("password",signInPassword.getText().toString());
        params.put("email",signInMail.getText().toString());

        return params;
    }

    @Override
    public void onClickOK(int code) {

    }

    @Override
    public void onClickCancel(int code) {

    }

    @Override
    public void onClickDatePickter(int year, int monthOfYear, int dayOfMonth) {

    }

    @Override
    public void onClickTimePicker(int hourOfDay, int minute) {

    }
}
