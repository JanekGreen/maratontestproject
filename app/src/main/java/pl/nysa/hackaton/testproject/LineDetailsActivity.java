package pl.nysa.hackaton.testproject;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.nysa.hackaton.testproject.Entity.Line;

public class LineDetailsActivity extends AppCompatActivity {

    @BindView(R.id.circleImg2)
    CardView lineCircle;
    @BindView(R.id.cardBG)
    CardView cardBG;
    private Line line;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_line_details);
        ButterKnife.bind(this);
        supportPostponeEnterTransition();
        Bundle extras = getIntent().getExtras();
        line =  extras.getParcelable("EXTRA_LINE_ITEM");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            lineCircle.setBackgroundTintList(this.getResources().getColorStateList(line.getLineColor()));
            cardBG.setBackgroundTintList(this.getResources().getColorStateList(line.getLineColor()));
        }




        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            String imageTransitionName = extras.getString("EXTRA_ANIMAL_IMAGE_TRANSITION_NAME");
            lineCircle.setTransitionName(imageTransitionName);
        }

        supportStartPostponedEnterTransition();

    }
}
