package pl.nysa.hackaton.testproject.interfaces;

import android.view.View;

import pl.nysa.hackaton.testproject.Entity.Activity;

/**
 * Created by akufa on 2017-10-29.
 */

public interface ListInterface {
    public void onItemClick(Activity activity, View v);
}
