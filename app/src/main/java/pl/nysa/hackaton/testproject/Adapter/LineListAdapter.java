package pl.nysa.hackaton.testproject.Adapter;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.nysa.hackaton.testproject.Entity.Line;
import pl.nysa.hackaton.testproject.interfaces.ListListener;
import pl.nysa.hackaton.testproject.R;

/**
 * Created by artur on 25.10.2017.
 */

public class LineListAdapter extends RecyclerView.Adapter<LineListAdapter.ViewHolder> {
    Context context;
    List<Line> listOfLines;
    ListListener listClickInterface;

    public LineListAdapter(List<Line> listLines, ListListener listClickInterface, Context context) {
        this.context = context;
        this.listOfLines = listLines;
        this.listClickInterface = listClickInterface;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_event_list, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Line line = listOfLines.get(position);
//        holder.lineNumber.setText(line.getNumber());
//        holder.lineAddress.setText(line.getAddress());
//        holder.lineIime.setText(line.getStartTime());
//        holder.lineDistance.setText(line.getDistance());
        ViewCompat.setTransitionName(holder.lineCircle, line.getAddress());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
          line.setLineColor(randomizeColor(holder));
        }
    }

    @Override
    public int getItemCount() {
        if(listOfLines == null)
            return 0;
        else
            return listOfLines.size();
    }

    /**
     *  stare metody, bo nowe wymagają androida M
     * @param holder
     */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private int randomizeColor(ViewHolder holder){
        int []colorIds = {R.color.random_line_color_1,
                R.color.random_line_color_2,
                R.color.random_line_color_3,
                R.color.random_line_color_4,
                R.color.random_line_color_5,
        };

        int rand = new Random().nextInt(5);
        holder.lineCircle.setBackgroundTintList(context.getResources().getColorStateList(colorIds[rand]));
        if(rand >= 3)
            holder.schoolTitle.setTextColor(context.getResources().getColor(R.color.iconWhite));

        return colorIds[rand];

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.circleImg2)
        CardView lineCircle;
        @BindView(R.id.schoolTitle)
        TextView schoolTitle;
        @BindView(R.id.eventDescription)
        TextView eventDescription;
        @BindView(R.id.eventTime)
        TextView eventTime;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listClickInterface.onItemClick(listOfLines.get(getAdapterPosition()), lineCircle);
        }
    }

}
