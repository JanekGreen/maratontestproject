package pl.nysa.hackaton.testproject.interfaces;

import com.android.volley.AuthFailureError;
import com.android.volley.VolleyError;

import java.util.Map;

/**
 * Created by wojci on 28.10.2017.
 */

public interface RequestInterface {
    void onResponse(String response);
    void onErrorResponse(VolleyError error);
    Map<String, String> getParams() throws AuthFailureError;
}
