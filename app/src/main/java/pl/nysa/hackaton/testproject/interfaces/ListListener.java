package pl.nysa.hackaton.testproject.interfaces;

import android.support.v7.widget.CardView;
import android.view.View;

import pl.nysa.hackaton.testproject.Entity.Line;

/**
 * Created by artur on 25.10.2017.
 */

public interface ListListener {

    void onItemClick(Line line, CardView view);
}
