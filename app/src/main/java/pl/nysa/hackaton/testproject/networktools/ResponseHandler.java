package pl.nysa.hackaton.testproject.networktools;

import android.provider.ContactsContract;

import com.db4o.ObjectSet;
import com.db4o.query.Query;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import pl.nysa.hackaton.testproject.Entity.Activity;
import pl.nysa.hackaton.testproject.Entity.Event;
import pl.nysa.hackaton.testproject.Entity.EventType;
import pl.nysa.hackaton.testproject.Entity.School;
import pl.nysa.hackaton.testproject.Entity.SchoolType;
import pl.nysa.hackaton.testproject.Entity.User;
import pl.nysa.hackaton.testproject.Entity.WeekDay;
import pl.nysa.hackaton.testproject.database.DatabaseHandler;

/**
 * Created by wojci on 29.10.2017.
 */

public class ResponseHandler {


    public static ArrayList<User> getUsers(String response){
        Gson gson = new Gson();
        return gson.fromJson(response, new TypeToken<ArrayList<User>>(){}.getType());
    }
    public static ArrayList<Event> getEvents(String response){
        Gson gson = new Gson();
        return gson.fromJson(response, new TypeToken<ArrayList<Event>>(){}.getType());
    }
    public static ArrayList<Activity> getActivities(String response){
        Gson gson = new Gson();
        return gson.fromJson(response, new TypeToken<ArrayList<Activity>>(){}.getType());
    }
    public static ArrayList<School> getSchools(String response){
        Gson gson = new Gson();
        return gson.fromJson(response, new TypeToken<ArrayList<School>>(){}.getType());
    }
    public static ArrayList<SchoolType> getSchoolSchoolTypes(String response){
        Gson gson = new Gson();
        return gson.fromJson(response, new TypeToken<ArrayList<SchoolType>>(){}.getType());
    }
    public static ArrayList<WeekDay> getWeekDays(String response){
        Gson gson = new Gson();
        return gson.fromJson(response, new TypeToken<ArrayList<WeekDay>>(){}.getType());
    }
    public static ArrayList<EventType> getEventTypes(String response){
        Gson gson = new Gson();
        return gson.fromJson(response, new TypeToken<ArrayList<EventType>>(){}.getType());
    }
}
